自定义列表 cell （UITableViewCell）的选中颜色（可增添渐变颜色），以及自定义cell和cell之间的分割线（separator）。

仅支持ARC模式。如果你的项目使用非ARC，则必须在编译模式中给此类库的所有代码加上：-fobjc-arc。	
